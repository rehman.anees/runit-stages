STAGES = \
	scripts/1 \
	scripts/2 \
	scripts/3

HELPERFUNC = \
	scripts/functions

all: install

install:
	install -d ${DESTDIR}/etc/sv
	install -d ${DESTDIR}/etc/runit/runsvdir
	install -d ${DESTDIR}/etc/runit/core-services
	install -m644 core-services/*.sh ${DESTDIR}/etc/runit/core-services
	install -m755 ${STAGES} ${DESTDIR}/etc/runit
	install -m644 ${HELPERFUNC} ${DESTDIR}/etc/runit
	cp -R --no-dereference --preserve=mode,links -v runsvdir/* ${DESTDIR}/etc/runit/runsvdir/
	cp -R --no-dereference --preserve=mode,links -v sv/* ${DESTDIR}/etc/sv/
	cp -R --no-dereference --preserve=mode,links -v power/* ${DESTDIR}/etc/runit

clean:
	@echo "Dummy clean"

.PHONY: all install clean
